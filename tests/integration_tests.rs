#[cfg(test)]
mod tests {
    use crossbeam_channel::{unbounded, Receiver, Sender};
    use serde::Deserialize;
    use serde_json::Value;
    use std::{
        fs::{read_to_string, File},
        io::{BufRead, BufReader},
        path::Path,
    };
    use supercell_assignment::{
        process_single_thread_channel,
        state::{Broadcast, UserFriendStore, UserStateStore},
    };

    #[test]
    fn ex1_test1() {
        test_helper(1)
    }
    #[test]
    fn ex1_test2() {
        test_helper(2)
    }
    #[test]
    fn ex1_test3() {
        test_helper(3)
    }

    #[test]
    fn ex2_test1() {
        let test_input = format!("./tests/ex2/input1.txt");
        let test_file = Path::new(&test_input);
        let test_out = format!("./tests/ex2/output1.txt");

        let expected_states =
            serde_json::from_str::<Value>(&read_to_string(test_out).unwrap()).unwrap();
        let (_, user_states, _) = run_single_thread(test_file);

        let all_states_match = user_states.0.iter().fold(true, |acc, (user_id, state)| {
            println!("user: {}", user_id);
            let expected_user_state = expected_states.get(user_id).unwrap();
            acc && state.store.iter().fold(true, |acc, s| {
                println!("  state {}", s.0);
                println!("    {}<>{}", s.1 .1, expected_user_state.get(s.0).unwrap());
                acc && expected_user_state.get(s.0).unwrap() == &s.1 .1
            })
        });

        assert!(all_states_match);
    }

    fn test_helper(test: u32) {
        let test_input = format!("./tests/ex1/input{test}.txt");
        let test_file = Path::new(&test_input);
        let test_out = format!("./tests/ex1/output{test}.txt");

        let expected_out = load_from_str::<Broadcast>(&test_out);
        let (actual_out, _, _) = run_single_thread(test_file);

        itertools::assert_equal(expected_out, actual_out);
    }

    fn run_single_thread(test_file: &Path) -> (Vec<Broadcast>, UserStateStore, UserFriendStore) {
        let (tx_command, rx_command): (Sender<String>, Receiver<String>) = unbounded();
        let (tx_broadcast, rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();

        let file = File::open(test_file).unwrap();
        for msg in BufReader::new(file).lines() {
            tx_command.send(msg.unwrap());
        }
        std::mem::drop(tx_command);

        let (user_states, user_friends) = process_single_thread_channel(rx_command, tx_broadcast);

        let mut actual_out = vec![];
        while let Some(b) = rx_broadcast.iter().next() {
            actual_out.push(b);
        }
        (actual_out, user_states, user_friends)
    }

    fn load_from_str<T: for<'a> Deserialize<'a>>(file: &str) -> Vec<T> {
        let file = std::fs::File::open(file).unwrap();
        let mut commands = Vec::new();
        for line in std::io::BufReader::new(file).lines() {
            let res = serde_json::from_str::<T>(&line.unwrap());
            match res {
                Err(e) => println!("{}", e),
                Ok(command) => {
                    commands.push(command);
                }
            }
        }
        commands
    }
}
