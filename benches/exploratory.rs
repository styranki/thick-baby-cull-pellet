use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use rayon::prelude::*;
use std::default;
use std::fs::File;
use std::io::{BufRead, BufReader};
use supercell_assignment::state::Command;

const INPUT_FILE: &str = "./tests/ex2/input1.txt";
fn read_to_string() -> Vec<String> {
    std::fs::read_to_string(INPUT_FILE)
        .unwrap()
        .split("\n")
        .map(|s| s.to_string())
        .collect()
}
fn buffered_read() -> Vec<String> {
    let mut res = vec![];
    let file = File::open(INPUT_FILE).unwrap();
    for line in BufReader::new(file).lines() {
        res.push(line.unwrap());
    }
    res
}

fn file_reading(c: &mut Criterion) {
    c.bench_function("File to string", |b| b.iter(|| read_to_string()));
    c.bench_function("File to Vec<String>", |b| b.iter(|| buffered_read()));
}

fn deserialize_string(input: &Vec<String>) -> Vec<Command> {
    input
        .iter()
        .map(|line| serde_json::from_str(&line).unwrap())
        .collect()
}

fn deserialize_str() -> Vec<Command> {
    std::fs::read_to_string(INPUT_FILE)
        .unwrap()
        .strip_suffix("\n")
        .unwrap()
        .split("\n")
        .map(|line| serde_json::from_str(line).unwrap())
        .collect()
}

fn deserialize_multi_thread(input: &Vec<String>) -> Vec<Command> {
    input
        .par_iter()
        .map(|line| serde_json::from_str(&line).unwrap())
        .collect()
}

fn deserialization(c: &mut Criterion) {
    let input_data = buffered_read();
    c.bench_function("Deserialize Vec<String>", |b| {
        b.iter(|| deserialize_string(&input_data))
    });
    c.bench_function("Deserialize read_to_str", |b| b.iter(|| deserialize_str()));

    let mut group = c.benchmark_group("Multi thread deserialization");
    let threads = [1, 2, 4, 6, 12, 24];
    for thread_count in threads {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(thread_count)
            .build()
            .unwrap();
        group.throughput(criterion::Throughput::Elements(input_data.len() as u64));
        group.sampling_mode(criterion::SamplingMode::Linear);
        group.bench_with_input(
            BenchmarkId::new("Deserialize threads", thread_count),
            &thread_count,
            |b, _input| b.iter(|| pool.install(|| deserialize_multi_thread(&input_data))),
        );
    }
    group.finish()
}

criterion_group! {
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = file_reading, deserialization
}

criterion_main!(benches);
