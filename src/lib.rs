use std::{
    collections::{HashMap, HashSet},
    sync::{Arc, RwLock},
};

use crossbeam::sync::ShardedLock;
use crossbeam_channel::{Receiver, Sender};
use state::{
    Broadcast, Command, LastUpdatedTimestamp, UserFriendStore, UserState, UserStateStore,
    UserStateValues,
};

pub mod state;

pub fn process_single_thread_vec(
    commands: &Vec<String>,
    sender: Sender<Broadcast>,
) -> (UserStateStore, UserFriendStore) {
    let mut user_states = UserStateStore::default();
    let mut user_friends = UserFriendStore::default();

    for command in commands.iter() {
        match serde_json::from_str::<Command>(&command).unwrap() {
            Command::MakeFriends { user1, user2 } => {
                user_friends.make_friends(user1, user2);
            }
            Command::DeleteFriend { user1, user2 } => {
                user_friends.unfriend_users(user1, user2);
            }
            Command::Update {
                user,
                timestamp,
                values,
            } => {
                if let Some(msg) =
                    handle_state_update(user, timestamp, values, &user_friends, &mut user_states)
                {
                    sender.send(msg).unwrap();
                }
            }
        }
    }

    (user_states, user_friends)
}

pub fn single_thread_vec(
    mut commands: Vec<Command>,
    sender: Sender<Broadcast>,
) -> (UserStateStore, UserFriendStore) {
    let mut user_states = UserStateStore::default();
    let mut user_friends = UserFriendStore::default();

    for command in commands.drain(0..) {
        match command {
            Command::MakeFriends { user1, user2 } => {
                user_friends.make_friends(user1, user2);
            }
            Command::DeleteFriend { user1, user2 } => {
                user_friends.unfriend_users(user1, user2);
            }
            Command::Update {
                user,
                timestamp,
                values,
            } => {
                if let Some(msg) =
                    handle_state_update(user, timestamp, values, &user_friends, &mut user_states)
                {
                    sender.send(msg).unwrap();
                }
            }
        }
    }

    (user_states, user_friends)
}
pub fn process_single_thread_channel(
    receiver: Receiver<String>,
    sender: Sender<Broadcast>,
) -> (UserStateStore, UserFriendStore) {
    let mut user_states = UserStateStore::default();
    let mut user_friends = UserFriendStore::default();

    while let Some(msg) = receiver.iter().next() {
        match serde_json::from_str::<Command>(&msg).unwrap() {
            Command::MakeFriends { user1, user2 } => {
                user_friends.make_friends(user1, user2);
            }
            Command::DeleteFriend { user1, user2 } => {
                user_friends.unfriend_users(user1, user2);
            }
            Command::Update {
                user,
                timestamp,
                values,
            } => {
                if let Some(msg) =
                    handle_state_update(user, timestamp, values, &user_friends, &mut user_states)
                {
                    sender.send(msg).unwrap();
                }
            }
        }
    }

    (user_states, user_friends)
}

pub fn process_multi_thread_channel(
    receiver: Receiver<String>,
    sender: Sender<Broadcast>,
    user_states: Arc<RwLock<UserStateStore>>,
    user_friends: Arc<RwLock<UserFriendStore>>,
) {
    while let Some(msg) = receiver.iter().next() {
        match serde_json::from_str::<Command>(&msg).unwrap() {
            Command::MakeFriends { user1, user2 } => {
                let mut user_friends = user_friends.write().unwrap();
                user_friends.make_friends(user1, user2);
            }
            Command::DeleteFriend { user1, user2 } => {
                let mut user_friends = user_friends.write().unwrap();
                user_friends.unfriend_users(user1, user2);
            }
            Command::Update {
                user,
                timestamp,
                values,
            } => {
                if let Some(msg) = {
                    let mut user_states = user_states.write().unwrap();
                    let user_friends = user_friends.read().unwrap();
                    handle_state_update(user, timestamp, values, &user_friends, &mut user_states)
                } {
                     sender.send(msg).unwrap()
                }
            }
        }
    }
}

pub fn process_multi_thread_shard(
    receiver: Receiver<Command>,
    sender: Sender<Broadcast>,
    user_friends: Arc<ShardedLock<UserFriendStore>>,
) -> UserStateStore {
    let mut user_states = UserStateStore::default();
    while let Some(command) = receiver.iter().next() {
        match command {
            Command::MakeFriends { user1, user2 } => {
                let mut user_friends = user_friends.write().unwrap();
                user_friends.make_friends(user1, user2);
            }
            Command::DeleteFriend { user1, user2 } => {
                let mut user_friends = user_friends.write().unwrap();
                user_friends.unfriend_users(user1, user2);
            }
            Command::Update {
                user,
                timestamp,
                values,
            } => {
                if let Some(msg) = {
                    let user_friends = user_friends.read().unwrap();
                    handle_state_update(user, timestamp, values, &user_friends, &mut user_states)
                } {
                    sender.send(msg).unwrap()
                }
            }
        }
    }
    user_states
}

fn handle_state_update(
    user_id: String,
    timestamp: LastUpdatedTimestamp,
    mut values: HashMap<String, String>,
    friend_store: &UserFriendStore,
    state_store: &mut UserStateStore,
) -> Option<Broadcast> {
    let current_state = state_store
        .0
        .entry(user_id.clone())
        .or_insert(UserState(HashMap::new()));

    let mut pairs_to_update: Vec<(String, String)> = values
        .drain()
        .filter(|(k, _)| {
            let Some(UserStateValues(last_updated, _)) = current_state.0.get(k) else { return true };

            if *last_updated < timestamp {
                true
            } else {
                false
            }
        })
        .collect();

    for (k, v) in pairs_to_update.iter().cloned() {
        current_state.0.insert(k, UserStateValues(timestamp, v));
    }

    let users_to_broadcast = friend_store
        .0
        .get(&user_id)
        .map_or_else(|| HashSet::new(), |e| e.0.iter().cloned().collect());

    let should_broadcast = !users_to_broadcast.is_empty() && !pairs_to_update.is_empty();

    return if should_broadcast {
        Some(Broadcast {
            broadcast: users_to_broadcast,
            user: user_id.clone(),
            timestamp,
            values: pairs_to_update.drain(0..).collect(),
        })
    } else {
        None
    };
}
