use std::{
    collections::HashMap,
    env,
    fs::File,
    io::{BufRead, BufReader},
    ops::Deref,
    sync::{Arc, RwLock},
    time::Instant,
};

use crossbeam::sync::ShardedLock;
use crossbeam_channel::{unbounded, Receiver, Sender};
use rayon::prelude::*;
use supercell_assignment::{
    process_multi_thread_shard,
    state::{Broadcast, Command, UserFriendStore, UserStateStore},
};

fn main() {
    let start = Instant::now();
    let args: Vec<String> = env::args().collect();
    let num_threads = args.get(2).map_or(1, |val| val.parse().unwrap());
    rayon::ThreadPoolBuilder::new()
        .num_threads(num_threads)
        .build_global()
        .unwrap();
    let (tx_broadcast, _rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();
    let user_friends = Arc::new(ShardedLock::new(UserFriendStore::default()));
    let combined_user_state = Arc::new(RwLock::new(UserStateStore::default()));

    let users = ["ab", "bc", "cd", "de", "ef", "fg", "gh", "hi", "ij", "jk"];
    let mut handles = vec![];
    let mut user_tx_handles = HashMap::new();
    for user in users {
        let user_friends = user_friends.clone();
        let (tx_command, rx_command): (Sender<Command>, Receiver<Command>) = unbounded();
        let tx_broadcast = tx_broadcast.clone();
        let combined_user_state = combined_user_state.clone();
        handles.push(std::thread::spawn(move || {
            let user_states = process_multi_thread_shard(rx_command, tx_broadcast, user_friends);
            for (key, value) in user_states.0.iter() {
                combined_user_state.write().unwrap().0.insert(key.to_string(), value.clone());
            }
        }));
        user_tx_handles.insert(user, tx_command);
    }

    let setup = Instant::now();

    let mut command_strings = vec![];
    let file = File::open(&args[1]).unwrap();
    for line in BufReader::new(file).lines() {
        command_strings.push(line.unwrap());
    }
    let file_read = Instant::now();

    {
        let tx_handles = Arc::new(user_tx_handles);
        command_strings.par_iter().for_each(|line| {
            let command = serde_json::from_str::<Command>(&line).unwrap();
            let user: &str = match command {
                Command::MakeFriends { ref user1, .. } => user1,
                Command::DeleteFriend { ref user1, .. } => user1,
                Command::Update { ref user, .. } => user,
            };
            let handle = tx_handles.deref().get(user).unwrap();
            handle.send(command).unwrap();
        });
    }
    let deserialize = Instant::now();

    for h in handles {
        match h.join() {
            Ok(_) => {}
            Err(e) => println!("{:?}", e),
        };
    }

    let state_lock = &combined_user_state.read().unwrap().0;
    println!("{}", serde_json::to_string(state_lock).unwrap());
    let process_finished = Instant::now();
    /*
    dbg!(setup.checked_duration_since(start));
    dbg!(file_read.checked_duration_since(setup));
    dbg!(deserialize.checked_duration_since(file_read));
    dbg!(process_finished.checked_duration_since(deserialize));
    dbg!(process_finished.checked_duration_since(start));
    */
}
