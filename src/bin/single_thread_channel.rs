use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    time::Instant,
};

use crossbeam_channel::{unbounded, Receiver, Sender};
use supercell_assignment::{
    process_single_thread_channel, state::Broadcast,
};

fn main() {
    let start = Instant::now();
    let args: Vec<String> = env::args().collect();

    let (tx_command, rx_command): (Sender<String>, Receiver<String>) = unbounded();
    let (tx_broadcast, rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();

    let handle = std::thread::spawn(|| {
        process_single_thread_channel(rx_command, tx_broadcast);
    });


    let file = File::open(&args[1]).unwrap();
    for msg in BufReader::new(file).lines() {
        tx_command.send(msg.unwrap()).unwrap();
    }
    std::mem::drop(tx_command);
    let file_read = Instant::now();
    while let Some(msg) = rx_broadcast.iter().next() {
        println!("{}", serde_json::to_string(&msg).unwrap());
    }
    handle.join().unwrap();
    let process_finished = Instant::now();
    /*
    dbg!(file_read.checked_duration_since(start));
    dbg!(process_finished.checked_duration_since(file_read));
    dbg!(process_finished.checked_duration_since(start));
    */
}
