use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    time::Instant,
};

use crossbeam_channel::{unbounded, Receiver, Sender};
use supercell_assignment::{
    process_single_thread_vec, state::Broadcast,
};

fn main() {
    let start = Instant::now();
    let args: Vec<String> = env::args().collect();
    let (tx_broadcast, rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();

    let file = File::open(&args[1]).unwrap();
    let mut commands = vec![];
    for msg in BufReader::new(file).lines() {
        commands.push(msg.unwrap());
    }
    let file_read = Instant::now();

    process_single_thread_vec(&commands, tx_broadcast);

    for msg in rx_broadcast.iter() {
        dbg!("{}", msg);
    }

    let process_finished = Instant::now();
    dbg!(file_read.checked_duration_since(start));
    dbg!(process_finished.checked_duration_since(file_read));
    dbg!(process_finished.checked_duration_since(start));

}
