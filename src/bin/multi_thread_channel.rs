use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    time::Instant, sync::{Arc, RwLock},
};

use crossbeam_channel::{unbounded, Receiver, Sender};
use supercell_assignment::{
    process_multi_thread_channel, state::{Broadcast, UserStateStore, UserFriendStore},
};


fn main() {
    let start = Instant::now();
    let args: Vec<String> = env::args().collect();

    let (tx_command, rx_command): (Sender<String>, Receiver<String>) = unbounded();
    let (tx_broadcast, rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();

    let user_states = Arc::new(RwLock::new(UserStateStore::default()));
    let user_friends = Arc::new(RwLock::new(UserFriendStore::default()));

    let mut handles = vec![];
    let num_threads = args.get(2).map_or(1, |val| val.parse().unwrap());
    for _ in 0..num_threads {
        let user_states = user_states.clone();
        let user_friends = user_friends.clone();
        let rx_command = rx_command.clone();
        let tx_broadcast = tx_broadcast.clone();
        handles.push(std::thread::spawn(move || {
            process_multi_thread_channel(rx_command, tx_broadcast, user_states, user_friends);
        }));
    }

    let file = File::open(&args[1]).unwrap();
    for msg in BufReader::new(file).lines() {
        tx_command.send(msg.unwrap()).unwrap();
    }
    std::mem::drop(tx_command);
    let file_read = Instant::now();

    for h in handles {
        match h.join() {
            Ok(_) => {},
            Err(e) => println!("{:?}", e),
        };
    }

    let process_finished = Instant::now();
    dbg!(file_read.checked_duration_since(start));
    dbg!(process_finished.checked_duration_since(file_read));
    dbg!(process_finished.checked_duration_since(start));
}
