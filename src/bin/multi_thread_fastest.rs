use std::{
    env,
    fs::File,
    io::{BufRead, BufReader},
    time::Instant, sync::{Arc, RwLock},
};

use crossbeam_channel::{unbounded, Receiver, Sender};
use supercell_assignment::{
    single_thread_vec, state::{Broadcast, UserStateStore, UserFriendStore, Command},
};
use rayon::prelude::*;


fn main() {
    let start = Instant::now();
    let args: Vec<String> = env::args().collect();
    let num_threads = args.get(2).map_or(1, |val| val.parse().unwrap());
    rayon::ThreadPoolBuilder::new().num_threads(num_threads).build_global().unwrap();
    let (tx_broadcast, rx_broadcast): (Sender<Broadcast>, Receiver<Broadcast>) = unbounded();

    let setup = Instant::now();

    let mut command_strings = vec![];
    let file = File::open(&args[1]).unwrap();
    for line in BufReader::new(file).lines() {
        command_strings.push(line.unwrap());
    }
    let file_read = Instant::now();

    let commands = command_strings.par_iter().map(|line| {
        serde_json::from_str::<Command>(&line).unwrap()
    }).collect();
    let deserialize = Instant::now();

    single_thread_vec(commands, tx_broadcast);


    let process_finished = Instant::now();
    dbg!(setup.checked_duration_since(start));
    dbg!(file_read.checked_duration_since(setup));
    dbg!(deserialize.checked_duration_since(file_read));
    dbg!(process_finished.checked_duration_since(deserialize));
    dbg!(process_finished.checked_duration_since(start));
}
