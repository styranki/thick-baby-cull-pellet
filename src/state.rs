use serde::{Deserialize, Serialize, Serializer};
use std::collections::{HashMap, HashSet};

pub type LastUpdatedTimestamp = u32;
#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type")]
pub enum Command {
    #[serde(rename = "make_friends")]
    MakeFriends { user1: String, user2: String },
    #[serde(rename = "del_friends")]
    DeleteFriend { user1: String, user2: String },
    #[serde(rename = "update")]
    Update {
        user: String,
        timestamp: u32,
        values: HashMap<String, String>,
    },
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Broadcast {
    pub broadcast: HashSet<String>,
    pub user: String,
    pub timestamp: LastUpdatedTimestamp,
    pub values: HashMap<String, String>,
}


#[derive(Serialize, Debug, Default, Clone)]
pub struct UserState (
     pub HashMap<String, UserStateValues>,
);

#[derive(Clone, Debug, Default)]
pub struct UserStateValues (pub LastUpdatedTimestamp, pub String);

impl Serialize for UserStateValues
where
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.1)
    }
}

#[derive(Debug, Default)]
pub struct UserStateStore(pub HashMap<String, UserState>);

#[derive(Debug, Default)]
pub struct UserFriends(pub HashSet<String>);

#[derive(Debug, Default)]
pub struct UserFriendStore(pub HashMap<String, UserFriends>);

impl UserFriendStore {
    pub fn make_friends(&mut self, user1: String, user2: String) -> bool {
        self.add_friend(user1.clone(), user2.clone()) || self.add_friend(user2, user1)
    }
    pub fn unfriend_users(&mut self, user1: String, user2: String) -> bool {
        self.delete_friend(user1.clone(), &user2) || self.delete_friend(user2, &user1)
    }

    fn add_friend(&mut self, user: String, new_friend: String) -> bool {
        let friend_struct = self.0.entry(user).or_insert(UserFriends(HashSet::new()));
        friend_struct.0.insert(new_friend)
    }

    fn delete_friend(&mut self, user: String, old_friend: &String) -> bool {
        let state = self.0.entry(user).or_insert(UserFriends(HashSet::new()));
        state.0.remove(old_friend)
    }
}
